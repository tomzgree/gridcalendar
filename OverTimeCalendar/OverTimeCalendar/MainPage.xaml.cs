﻿using OverTimeCalendar.Models;
using OverTimeCalendar.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using XLabs.Forms.Controls;
using XLabs.Ioc;

namespace OverTimeCalendar
{


    public partial class MainPage : ContentPage
    {
        private readonly ICalendarService _calendarService;
        private IEnumerable<Grid> _list;

        public MainPage(ICalendarService calendarService)
        {
            _calendarService = calendarService;

            InitializeComponent();

            _list = _calendarService.Gridder();

            gridListView.ItemTemplate = new DataTemplate(() =>
            {

                foreach (var item in _list)
                {
                    return new ViewCell { View = item  };
                }


                return new ViewCell { View = null };


           });

            gridListView.ItemsSource = _list;

        }



    }
}



//foreach (var days in _list)
//{


//    var gridCalendar = new Grid();
//    var label = new Label { TextColor = Color.White };
//    //Rows
//    gridCalendar.RowDefinitions.Add(new RowDefinition { Height = 30 });
//    gridCalendar.RowDefinitions.Add(new RowDefinition { Height = 50 });
//    gridCalendar.RowDefinitions.Add(new RowDefinition { Height = 50 });
//    gridCalendar.RowDefinitions.Add(new RowDefinition { Height = 50 });
//    gridCalendar.RowDefinitions.Add(new RowDefinition { Height = 50 });
//    gridCalendar.RowDefinitions.Add(new RowDefinition { Height = 50 });

//    gridCalendar.ColumnSpacing = 1;
//    gridCalendar.RowSpacing = 1;
//    gridCalendar.BackgroundColor = Color.Silver;


//    label.Text = days.Name;

//    gridCalendar.Children.Add(label, 0, 0);
//    Grid.SetColumnSpan(label, 7);




//    for (int i = 0; i < days.Days.Length; i++)
//    {
//        if (i >= 0 && i <= 6)
//        {
//            gridCalendar.Children.Add(new Label { Text = String.Format("{0:dd}", days.Days[i]), BackgroundColor = Color.White }, i, 1);
//        }
//        else if (i >= 7 && i <= 13)
//        {
//            gridCalendar.Children.Add(new Label { Text = String.Format("{0:dd}", days.Days[i]), BackgroundColor = Color.White }, i - 7, 2);
//        }
//        else if (i >= 14 && i <= 20)
//        {
//            gridCalendar.Children.Add(new Label { Text = String.Format("{0:dd}", days.Days[i]), BackgroundColor = Color.White }, i - 14, 3);
//        }
//        else if (i >= 21 && i <= 27)
//        {
//            gridCalendar.Children.Add(new Label { Text = String.Format("{0:dd}", days.Days[i]), BackgroundColor = Color.White }, i - 21, 4);
//        }
//        else if (i >= 28 && i <= 31)
//        {
//            gridCalendar.Children.Add(new Label { Text = String.Format("{0:dd}", days.Days[i]), BackgroundColor = Color.White }, i - 28, 5);
//        }


//    }

//    return new ViewCell { View = gridCalendar };
//}
