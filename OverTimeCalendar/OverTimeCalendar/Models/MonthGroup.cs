﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OverTimeCalendar.Models
{
    public class MonthGroup : List<Month>
    {
        public string MonthName { get; set; }

        public MonthGroup(string monthName)
        {
            MonthName = monthName;
        }
    }
}
