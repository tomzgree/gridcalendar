﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OverTimeCalendar.Models
{
    public class Month
    {
        private DateTime[] _days;

        public int Id { get; set; }
        public DateTime DateTime { get; set; }
        public string Name { get { return String.Format("{0:MMMM yyyy}", DateTime); } }
        public int DaysInMonth { get { return DateTime.DaysInMonth(DateTime.Year, DateTime.Month); } }
        public DateTime[] Days { get { return DaysArray(DaysInMonth); } }

        private DateTime[] DaysArray(int daysInMonth)
        {
            _days = new DateTime[daysInMonth];
            for (int i = 0; i < daysInMonth; i++)
            {
                _days[i] = new DateTime(DateTime.Year, DateTime.Month, i + 1);
            }
            return _days;

        }

    }
}
