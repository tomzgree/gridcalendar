﻿using OverTimeCalendar.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace OverTimeCalendar.Services
{
    public interface ICalendarService
    {
        IEnumerable<Month> GetMonth();
        List<Grid> Gridder();
    }
}
