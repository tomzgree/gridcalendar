﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OverTimeCalendar.Models;
using Xamarin.Forms;

namespace OverTimeCalendar.Services
{
    public class CalendarService : ICalendarService
    {
        public IEnumerable<Month> GetMonth()
        {
            //return new List<Month>()
            //{
            //     new Month { Id = 1, DateTime = DateTime.Now },
            //     new Month { Id = 2, DateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month + 1, 1)},
            //     new Month { Id = 3, DateTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month + 2, 1) }
            //};
            return CalendarGenerator();
        }

        private List<Month> CalendarGenerator()
        {
            var calendarList = new List<Month>();
            for (int i = 1; i < 12; i++)
            {
                calendarList.Add(new Month { Id = i, DateTime = new DateTime(DateTime.Now.Year, i, 1) });
            }

            return calendarList;
        }

        //TODO: List<Grid>
        // Init list of grids and labels
        //Populate List of grids with List<Month> some how...

        public List<Grid> Gridder()
        {
            var grids = new List<Grid>();
            var labels = new List<Label>();

            for (int j = 0; j < CalendarGenerator().Count(); j++)
            {
                grids.Add(new Grid());
                labels.Add(new Label());

                grids[j].RowDefinitions.Add(new RowDefinition { Height = 30 });
                grids[j].RowDefinitions.Add(new RowDefinition { Height = 50 });
                grids[j].RowDefinitions.Add(new RowDefinition { Height = 50 });
                grids[j].RowDefinitions.Add(new RowDefinition { Height = 50 });
                grids[j].RowDefinitions.Add(new RowDefinition { Height = 50 });
                grids[j].RowDefinitions.Add(new RowDefinition { Height = 50 });

                grids[j].ColumnSpacing = 1;
                grids[j].RowSpacing = 1;
                grids[j].BackgroundColor = Color.Silver;

                labels[j].Text = CalendarGenerator()[j].Name;

                grids[j].Children.Add(labels[j], 0, 0);
                Grid.SetColumnSpan(labels[j], 7);

                //Second loop
                for (int i = 0; i < CalendarGenerator()[j].Days.Length; i++)
                {
                    if (i >= 0 && i <= 6)
                    {
                        grids[j].Children.Add(new Label { Text = String.Format("{0:dd}", CalendarGenerator()[j].Days[i]), BackgroundColor = Color.White }, i, 1);
                    }
                    else if (i >= 7 && i <= 13)
                    {
                        grids[j].Children.Add(new Label { Text = String.Format("{0:dd}", CalendarGenerator()[j].Days[i]), BackgroundColor = Color.White }, i - 7, 2);
                    }
                    else if (i >= 14 && i <= 20)
                    {
                        grids[j].Children.Add(new Label { Text = String.Format("{0:dd}", CalendarGenerator()[j].Days[i]), BackgroundColor = Color.White }, i - 14, 3);
                    }
                    else if (i >= 21 && i <= 27)
                    {
                        grids[j].Children.Add(new Label { Text = String.Format("{0:dd}", CalendarGenerator()[j].Days[i]), BackgroundColor = Color.White }, i - 21, 4);
                    }
                    else if (i >= 28 && i <= 31)
                    {
                        grids[j].Children.Add(new Label { Text = String.Format("{0:dd}", CalendarGenerator()[j].Days[i]), BackgroundColor = Color.White }, i - 28, 5);
                    }


                }

            }
            
            return grids;

        }
    }
}
